var
  path = require('path'),
  BlockedResources = require('impress-blocked-resources'),
  HtmlAngularMinify = require('impress-html-angular-minify'),
  HtmlMinify = require('impress-html-minify'),
  MySqlStorage = require('impress-mysql-storage')
  ;

module.exports = function(impress) {

  impress
    .addConfig(
      path.join(__dirname, 'config/config.json'),
      path.join(__dirname, 'config/config.local.json'),
      true
    )
    .addBlockedResources(
      new BlockedResources()
    )
    .addHtmlFilters(
      new HtmlAngularMinify(),
      new HtmlMinify()
    )
    .addStorage(
      new MySqlStorage(impress.options.storage)
    );

};